package page.objects;

import driver.manager.DriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utils.UtilsMethods;
import wait.WaitFor;

import java.util.List;

public class MenuSection {

    @FindBy(xpath = "//a[contains(@class, '_s8izy _vc293') and text()='Smartfony i telefony komórkowe']")
    private WebElement categorySmartphonesAndPhones;

    @FindBy(xpath = "//*[@data-group-id='departments_Elektronika']")
    private WebElement categoryElectronics;

    @FindBy(xpath = "//a[@title='Telefony i Akcesoria']")
    private WebElement categoryPhoneAndAccesiories;

    @FindBy(xpath = "//a[contains(@class, '_s8izy _vc293') and text()='Apple']")
    private WebElement categoryApple;

    @FindBy(xpath = "//a[contains(@class, '_s8izy _vc293') and text()='iPhone 8']")
    private WebElement categoryIphone8;

    @FindBy(xpath = "//div[@class='opbox-listing--base']//article")
    private List<WebElement> itemsOnTheFirstPage;

    @FindBy(xpath = "//div[@class='opbox-listing--base']//article//span[@class='fee8042' and text()]")
    private List<WebElement> pricesOfTheElements;


    public MenuSection(WebDriver driver){
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public  MenuSection categoryElectronics(){
        WaitFor.waitForElementToBeClickable(categoryElectronics);
        categoryElectronics.click();

        String expectedTitle = "Elektronika użytkowa - Allegro.pl";
        String actualTitle = DriverManager.getWebDriver().getTitle();
        Assert.assertTrue(expectedTitle.equals(actualTitle));
        return this;
    }

    public MenuSection categoryPhoneAndAccesiories(){
        WaitFor.waitForElementToBeClickable(categoryPhoneAndAccesiories);
        categoryPhoneAndAccesiories.click();
        return this;
    }

    public MenuSection categorySmartphonesAndPhones(){
        WaitFor.waitForElementToBeClickable(categorySmartphonesAndPhones);
        categorySmartphonesAndPhones.click();

        String expectedTitle = "Najlepsze smartfony i telefony komórkowe - Allegro.pl";
        String actualTitle = DriverManager.getWebDriver().getTitle();
        Assert.assertTrue(expectedTitle.equals(actualTitle));
        return this;
    }

    public MenuSection categoryApple(){
        WaitFor.waitForElementToBeClickable(categoryApple);
        categoryApple.click();

        String expectedTitle = "Apple / iPhone - Smartfony / Telefony komórkowe - Allegro.pl";
        String actualTitle = DriverManager.getWebDriver().getTitle();
        Assert.assertTrue(expectedTitle.equals(actualTitle));
        return this;
    }

    public MenuSection countIPhonesOnFirstPage(){
        UtilsMethods utilsMethods = new UtilsMethods();
        utilsMethods.countingItems(itemsOnTheFirstPage);
        return this;
    }

    public MenuSection getHighestPrice(){
        UtilsMethods utilsMethods = new UtilsMethods();
        utilsMethods.getHighestValueofElements(pricesOfTheElements);
        return this;
    }

    public void addExtraToTheHighestPrice(){
        UtilsMethods utilsMethods = new UtilsMethods();
        utilsMethods.addExtraToTheValue(1.2);
    }

}
