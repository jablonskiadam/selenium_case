package page.objects;

import driver.manager.DriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import wait.WaitFor;

public class MainPage {

    @FindBy(xpath = "//*[@class='main-wrapper']//*[@class='_1h7wt _sjlwg']//*[@class='_13q9y _8hkto _nu6wg _1sql3 _qdoeh _l7nkx _nyhhx']")
    private WebElement informPopup;


    public MainPage(WebDriver driver){
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public void closePopup(){
        WaitFor.waitForElementToBeClickable(informPopup);
        informPopup.click();
    }

}
