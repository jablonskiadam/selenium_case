package tests;

import org.testng.annotations.Test;
import page.objects.MainPage;
import page.objects.MenuSection;
import utils.UtilsMethods;

public class MainTest extends Base{

    @Test
    public void testAllegro() {
        MainPage mainPage = new MainPage(driver);
        mainPage.closePopup();

        MenuSection menuSection = new MenuSection(driver);
        menuSection
                .categoryElectronics()
                .categoryPhoneAndAccesiories();

         UtilsMethods utilsMethods = new UtilsMethods();
         utilsMethods.switchTab();

         menuSection
                .categorySmartphonesAndPhones()
                .categoryApple()
                .countIPhonesOnFirstPage()
                .getHighestPrice()
                .addExtraToTheHighestPrice();
    }
}
