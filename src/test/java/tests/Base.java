package tests;

import driver.manager.DriverManager;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class Base {

    public WebDriver driver;

    @BeforeTest
    public void beforeTests(){

        driver = DriverManager.getWebDriver();
        driver.manage().window().maximize();
        driver.navigate().to("https://allegro.pl/");
        String pageTitle = driver.getTitle();
        Assert.assertTrue(pageTitle.contains("Allegro.pl – najlepsze ceny, największy wybór i zawsze bezpieczne zakupy online"));
    }

    @AfterTest
    public void afterTests(){
        DriverManager.closeDriver();
    }
}
