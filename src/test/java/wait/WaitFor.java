package wait;

import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitFor {

    private static WebDriverWait getWebDriverWait(){
        return new WebDriverWait(DriverManager.getWebDriver(),10);
    }

    public static void waitForElementToBeClickable(WebElement element){
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(element));
    }

}
