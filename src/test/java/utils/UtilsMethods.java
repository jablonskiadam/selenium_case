package utils;

import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UtilsMethods {

    private static double maxValue;

    public void switchTab(){
        ArrayList<String> tab2 = new ArrayList<String>(DriverManager.getWebDriver().getWindowHandles());
        DriverManager.getWebDriver().close();
        DriverManager.getWebDriver().switchTo().window(tab2.get(1));
    }

    public void countingItems(List<WebElement> items) {
        int i = 0;
        for (WebElement link : items) {
            items.add(link);
        }
        i = items.size();
        System.out.println("The amount of displayed iPhones8 on the first page: "+i);
    }

    public double getHighestValueofElements(List<WebElement> pricesOfItems){
        String replaceValue;
        double castValueToDouble;
        List<Double> xx = new ArrayList<Double>();
        for (WebElement link : pricesOfItems) {
             pricesOfItems.add(link);
             replaceValue = link.getText().replace(" ","").replace(",",".");
             castValueToDouble = Double.parseDouble(replaceValue.substring(0, replaceValue.length()-2));
             xx.add(castValueToDouble);
         }
        Collections.sort(xx);
        Collections.reverse(xx);
        this.maxValue = xx.get(0);
        System.out.println("The highest price on the first page: " + maxValue);
        return this.maxValue;
    }

    public void addExtraToTheValue(double extra){
        this.maxValue = maxValue *extra;
        System.out.println("The highest value with extra cost: " + this.maxValue);
    }
}